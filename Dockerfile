ARG VERSION

FROM buildpack-deps:$VERSION

ENV RUSTUP_HOME=/usr/local/rustup \
	CARGO_HOME=/usr/local/cargo \
	PATH=/usr/local/cargo/bin:$PATH

COPY --from=rust:latest /usr/local/rustup /usr/local/rustup
COPY --from=rust:latest /usr/local/cargo /usr/local/cargo

COPY --from=node:lts-slim /usr/local /usr/local

RUN cargo install cargo-deb && \
	rustup component add rustfmt clippy && \
    rustup target add aarch64-unknown-linux-gnu armv7-unknown-linux-gnueabihf && \
    apt-get update && \
    apt-get install -y gcc-aarch64-linux-gnu g++-aarch64-linux-gnu gcc-arm-linux-gnueabihf g++-arm-linux-gnueabihf

COPY cargo_config.toml /usr/local/cargo/config.toml
